require 'securerandom'
require 'sidekiq/web'
require 'rack/session'

Sidekiq.configure_client do |config|
  config.redis = { url: ENV.fetch('REDIS_URL'), size: 1 }
end

use Rack::Session::Cookie,
    secret: ENV.fetch('SESSION_KEY', SecureRandom.hex(32)),
    same_site: true,
    max_age: 86400

run Rack::URLMap.new({'/queues'=>Sidekiq::Web})
