SHELL = /bin/bash

repository ?= ddr-jobs

BUILD_TAG ?= $(repository):latest

CHART_VERSION ?= $(shell awk '/^version/ {print $$NF}' ./chart/Chart.yaml)

bundle = docker run --rm -v $(shell pwd):/app $(BUILD_TAG) bundle

.PHONY: build
build:
	./build.sh

.PHONY: console
console:
	docker run --rm -v "$(shell pwd):/app" -it $(BUILD_TAG) /bin/bash

.PHONY: test
test:
	$(bundle) exec rake -I lib

.PHONY: lock
lock:
	$(bundle) lock

.PHONY: update
update:
	$(bundle) update $(args)

.PHONY: audit
audit:
	docker run --rm -v "$(shell pwd):/app" $(BUILD_TAG) ./audit.sh

.PHONY: helm-update
helm-update:
	helm dependency update chart

.PHONY: helm-lint
helm-lint:
	helm lint chart --with-subcharts

.PHONY: chart
chart: helm-update helm-lint
