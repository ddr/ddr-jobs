require 'ddr/jobs/api'

use Rack::RewindableInput::Middleware

run Rack::URLMap.new({'/api'=>Ddr::Jobs::API})
