#
# Sidekiq
#
require 'sidekiq'

Sidekiq.configure_server do |config|
  config.logger = Ddr::Jobs.logger
end
