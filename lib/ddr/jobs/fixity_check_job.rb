require 'date'
require 'digest'

module Ddr::Jobs
  class FixityCheckJob < BaseJob
    def perform(resource_id)
      resource = Ddr::API.get_resource(resource_id)

      files = resource.fetch('files', {}).compact

      checked_at = DateTime.now

      details = {}

      files.each do |file_name, file_info|
        expected = file_info['sha1']
        error = nil
        outcome = 'success'

        begin
          actual = Digest::SHA1.file(file_info['file_path'])
          raise Error, "Expected: #{expected} / Actual: #{actual}" if actual != expected
        rescue Exception => e
          outcome = 'failure'
          error = e.message
        end

        details[file_name] = {outcome:, error:}.compact
      end

      data = {
        outcome: details.values.detect(proc {'success'}) { |v| v[:outcome] == 'failure' },
        checked_at: checked_at.iso8601,
        details: JSON.dump(details),
      }

      Ddr::API.post_resource_fixity_check(resource_id, data)
    end
  end
end
