require 'grape'
require 'grape-swagger'
require 'ddr/jobs'
require 'ddr/jobs/api/api_helpers'

module Ddr::Jobs
  class API < Grape::API

    UUID_RE = Regexp.new('\A\h{8}-\h{4}-\h{4}-\h{4}-\h{12}\z')

    MAX_RESOURCES = 100_000 # Arbitrary sanity check

    self.logger.level = ENV.fetch('API_LOG_LEVEL', 'debug')

    format :json

    helpers APIHelpers

    # POST /file_c14n
    desc 'Submit resource IDs for file characterization' do
      consumes ['application/json']
      success [{code: 202, message: 'Accepted'}]
    end
    params { use :resources }
    post :file_c14n do
      enqueue_jobs(job_class: Ddr::Jobs::FileC14nJob)
    end

    # POST /fixity_check
    desc 'Submit resource IDs for fixity checking' do
      consumes ['application/json']
      success [{code: 202, message: 'Accepted'}]
    end
    params { use :resources }
    post :fixity_check do
      enqueue_jobs(job_class: Ddr::Jobs::FixityCheckJob)
    end

    swagger_doc_params = {
      doc_version: ENV.fetch('CHART_VERSION', '0.1.0'),
      info: {
        title: 'DDR Jobs API',
        version: ENV.fetch('CHART_VERSION', '0.1.0'),
        contact_name: 'Library System Administration',
        contact_email: 'library-system-administration@duke.edu',
        license: 'Apache-2.0',
      },
      base_path: '/api',
      produces: ['application/json'],
      security_definitions: {
        oauth: {
          type: 'apiKey',
          in: 'header',
          name: 'Authorization'
        },
      },
      security: [{ oauth: [] }],
    }

    add_swagger_documentation(swagger_doc_params)
  end
end
