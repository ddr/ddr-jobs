require 'sidekiq/api'

module Ddr::Jobs
  module APIHelpers
    extend Grape::API::Helpers

    def logger
      API.logger
    end

    def send_response(code:, message:)
      error! message, code if code >= 400
      status code
      {code:, message:}
    end

    def queue_status(queue)
        enqueued = Sidekiq::Stats.new.queues.fetch(queue, 0)
        processing = Sidekiq::WorkSet.new.count { |_pid, _tid, work| work.payload['queue'] == queue }
        { enqueued:, processing: }
    end

    def enqueue_jobs(job_class:)
      @resource_ids = params[:resources].map { |r| r['id'] }
      num_resources = @resource_ids.length

      # No resources - 400 Bad Request
      if num_resources == 0
        send_response(code: 400, message: 'No resource IDs submitted.')
      end

      # Too many resources - 413 Content Too Large
      if num_resources > API::MAX_RESOURCES
        send_response(code: 413,
                      message: "Too many resources: #{num_resources} (max: #{MAX_RESOURCES})")
      end

      # Enqueue jobs - 202 Accepted
      job_class.set(queue: params[:priority]).perform_bulk(@resource_ids.zip)

      send_response(code: 202,
                    message: "#{@resource_ids.length} jobs have been submitted for processing.")
    end

    params :resources do
      requires :resources, type: Array[JSON] do
        requires :id, type: String, regexp: API::UUID_RE, allow_blank: false
      end

      optional :priority, type: String, allow_blank: false, values: Ddr::Jobs::PRIORITIES, default: 'default'
    end

  end
end
