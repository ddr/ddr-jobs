module Ddr::Jobs
  # @abstract
  class BaseJob
    include Sidekiq::Job

    sidekiq_options retry: 3, backtrace: true
  end
end
