require 'stringio'
require_relative 'fits_service'

module Ddr::Jobs
  class FileC14nJob < BaseJob
    # @param resource_id [String] DDR resource ID
    def perform(resource_id)
      resource = Ddr::API.get_resource(resource_id)

      file_path = resource.dig('files', 'content', 'file_path')

      raise Error, "Content file missing for resource #{resource_id}" unless file_path

      StringIO.open(FitsService.call(file_path), 'r') do |io|
        Ddr::API.post_resource_technical_metadata(resource_id, io)
      end
    end
  end
end
