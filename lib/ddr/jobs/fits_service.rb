module Ddr::Jobs
  class FitsService
    # @param file_path [String] path to file to examine
    # @return [String] FITS output XML
    # @raise [Ddr::Jobs::Error] if FITS service response is not a success
    def self.call(file_path)
      response = Net::HTTP.get_response(uri(file_path))
      raise Error, "FITS service error: #{response.code} #{response.message}" unless response.is_a?(Net::HTTPSuccess)
      response.body
    end

    # The URI for FITS request to examine the file path
    # @return [URI]
    def self.uri(file_path)
      URI(ENV.fetch('FITS_URL')).tap do |u|
        u.path += '/examine'
        u.query = URI.encode_www_form(file: file_path)
      end
    end
  end
end
