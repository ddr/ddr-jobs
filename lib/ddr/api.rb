require 'net/http/post/multipart'

module Ddr
  #
  # Static methods to call DDR APIs.
  #
  module API
    class Error < ::StandardError; end

    class << self
      def uri
        @uri ||= URI(ENV.fetch('DDR_API_URL', 'http://localhost:3000/api')).freeze
      end

      def url(path = '')
        uri.to_s + path
      end

      def url_path(path = '')
        uri.path + path
      end

      def get_resource(resource_id)
        path = url_path("/resources/#{resource_id}")
        req = Net::HTTP::Get.new(path, {'accept'=>'application/json'})
        response = request(req)
        JSON.parse(response.body)
      end

      def post_resource_technical_metadata(resource_id, io)
        upload_io = Multipart::Post::UploadIO.new(io, 'text/xml', 'fits_output.xml')
        req = Net::HTTP::Post::Multipart.new(url_path("/resources/#{resource_id}/technical_metadata"), {file: upload_io})
        request(req)
      end

      def post_resource_fixity_check(resource_id, data)
        req = Net::HTTP::Post.new(url_path("/resources/#{resource_id}/fixity_check"), {'content-type'=>'application/json'})
        req.body = JSON.dump(data)
        request(req)
      end

      private

      def auth
        'Bearer %s' % ENV.fetch('DDR_API_KEY', 'TOKEN_MISSING')
      end

      def request(req)
        response = Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
          req['authorization'] = auth
          http.request(req)
        end

        raise Error, "#{response.code} #{response.message}" unless response.is_a?(Net::HTTPSuccess)

        response
      end
    end
  end
end
