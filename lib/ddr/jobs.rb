require 'json'
require 'logger'
require 'net/http'
require_relative './api'

module Ddr
  module Jobs
    class Error < ::StandardError; end

    PRIORITIES = %w[ low default high ].freeze

    def self.logger
      @logger ||= Logger.new($stdout, level: ENV.fetch('LOG_LEVEL', 'info'))
    end
  end
end

require_relative 'jobs/config'
require_relative 'jobs/base_job'
require_relative 'jobs/fixity_check_job'
require_relative 'jobs/file_c14n_job'
