# ddr-jobs

Background jobs processing for the Duke Digital Repository

See `docker-compose.yml` for example stack.

Required environment variables:
  - DDR_API_URL
  - DDR_API_KEY
  - REDIS_URL
