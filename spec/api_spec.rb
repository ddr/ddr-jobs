require 'securerandom'
require 'json'
require 'ddr/jobs/api'

module Ddr::Jobs
  RSpec.describe API do
    include Rack::Test::Methods

    def app
      Ddr::Jobs::API
    end

    let(:id1) { SecureRandom.uuid }
    let(:id2) { SecureRandom.uuid }
    let(:id3) { SecureRandom.uuid }
    let(:resources) { [{ id: id1 }, { id: id2 }, { id: id3 }] }
    let(:data) { JSON.dump({resources:}) }
    let(:headers) { {'CONTENT_TYPE'=>'application/json'} }

    describe 'Swagger doc' do
      it 'is successful' do
        get '/swagger_doc.json', {'accept'=>'application/json'}
        expect(last_response.status).to eq 200
      end
    end

    describe 'POST /file_c14n' do
      before do
        allow_any_instance_of(Ddr::Jobs::FileC14nJob).to receive(:perform)
      end

      describe 'with a list of resource ids' do
        it 'is accepted' do
          post '/file_c14n', data, headers
          expect(last_response.status).to eq 202
        end

        describe 'that is empty' do
          let(:resources) { [] }

          it 'is a bad request' do
            post '/file_c14n', data, headers
            expect(last_response.status).to eq 400
          end
        end
      end
    end

    describe 'POST /fixity_check' do
      before do
        allow_any_instance_of(Ddr::Jobs::FixityCheckJob).to receive(:perform)
      end

      describe 'with a list of resource ids' do
        it 'is accepted' do
          post '/fixity_check', data, headers
          expect(last_response.status).to eq 202
        end

        describe 'that is empty' do
          let(:resources) { [] }

          it 'is a bad request' do
            post '/fixity_check', data, headers
            expect(last_response.status).to eq 400
          end
        end
      end
    end
  end
end
