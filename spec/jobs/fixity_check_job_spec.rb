module Ddr::Jobs
  RSpec.describe FixityCheckJob do

    let(:resource) { {'files'=>{'content'=>{'file_path'=>content_file_path, 'sha1'=>sha1}}} }
    let(:content_file_path) { '/not/a/real/path' }
    let(:sha1) { '0a012fd0b45b7079bdb3c2c8429c699ce96ef20d' }
    let(:resource_id) { SecureRandom.uuid }
    let(:outcome) { 'success' }

    before do
      @get_resource = stub_request(:get, Ddr::API.url("/resources/#{resource_id}"))
                        .to_return_json(body: JSON.dump(resource))
      @post_check = stub_request(:post, Ddr::API.url("/resources/#{resource_id}/fixity_check"))
                      .to_return(status: 200)
      allow(Digest::SHA1).to receive(:file).with(content_file_path).and_return(sha1)
    end

    after { WebMock.reset! }

    it 'is successful' do
      described_class.perform_async(resource_id)
      expect(@get_resource).to have_been_requested
      expect(@post_check).to have_been_requested
    end

  end
end
