module Ddr::Jobs
  RSpec.describe FileC14nJob do

    let(:resource) { {'files'=>{'content'=>{'file_path'=>content_file_path}}} }
    let(:content_file_path) { '/not/a/real/path' }
    let(:resource_id) { SecureRandom.uuid }
    let(:fits_xml) do
      <<-EOS
<?xml version="1.0" encoding="UTF-8"?>
<fits xmlns="http://hul.harvard.edu/ois/xml/ns/fits/fits_output" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hul.harvard.edu/ois/xml/ns/fits/fits_output http://hul.harvard.edu/ois/xml/xsd/fits/fits_output.xsd" version="1.5.0" timestamp="1/28/21, 3:12 PM">
</fits>
      EOS
    end

    before do
      @get_resource = stub_request(:get, Ddr::API.url("/resources/#{resource_id}"))
                        .to_return_json(body: JSON.dump(resource))
      @post_fits = stub_request(:post, Ddr::API.url("/resources/#{resource_id}/technical_metadata"))
                    .to_return(status: 200)
      allow(FitsService).to receive(:call).with(content_file_path).and_return(fits_xml)
    end

    after { WebMock.reset! }

    it 'is successful' do
      described_class.perform_async(resource_id)
      expect(@get_resource).to have_been_requested
      expect(@post_fits).to have_been_requested
    end

  end
end
