ARG ruby_version="3.1.4"

FROM docker.io/ruby:${ruby_version}

ARG APP_VERSION="development"
ARG BUILD_DATE="1970-01-01T00:00:00Z"

ENV APP_ENV="production" \
    HOME="/app" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    REDIS_URL="redis://redis:6379/0" \
    TZ="US/Eastern"

LABEL org.opencontainers.artifact.description="Duke Digital Repository background jobs application"
LABEL org.opencontainers.image.created="{BUILD_DATE}"
LABEL org.opencontainers.image.license="BSD-3-Clause"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/ddr/ddr-jobs"
LABEL org.opencontainers.image.url="https://repository.duke.edu"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.version="${APP_VERSION}"

RUN set -eux; \
    apt-get -y update; \
    apt-get -y install jq less libjemalloc2 locales; \
    rm -rf /var/lib/apt/lists/*; \
    echo "$LANG UTF-8" >> /etc/locale.gen; \
    locale-gen $LANG; \
    useradd -u 1001 -g 0 -d /app -s /sbin/nologin app

WORKDIR /app

COPY .ruby-version Gemfile Gemfile.lock ./

RUN set -eux; \
    gem install bundler -v "$(tail -1 Gemfile.lock | awk '{print $1}')"; \
    bundle install

COPY . .

RUN set -eux; \
    SIDEKIQ_VERSION=$(bundle exec sidekiq -V | awk '{print $NF}'); \
    mkdir -p web/views; \
    cp $GEM_HOME/gems/sidekiq-${SIDEKIQ_VERSION}/web/views/* ./web/views/; \
    sed -e 's|Sidekiq::NAME|ENV.fetch("APPLICATION_HOSTNAME")|' \
      -i ./web/views/layout.erb ./web/views/_nav.erb; \
    chmod -R g=u . $GEM_HOME; \
    git config --system --add safe.directory "*"

USER app

EXPOSE 9292
