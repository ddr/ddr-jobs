#!/bin/bash

docker build -t ${BUILD_TAG:-ddr-jobs:latest} \
       --build-arg ruby_version=$(cat .ruby-version) \
       --build-arg BUILD_DATE=$(date -I) \
       --build-arg APP_VERSION=${CI_COMMIT_SHA:-development} \
       .
